
import { SkTabImpl }  from '../../sk-tabs/src/impl/sk-tab-impl.js';

export class DefaultSkTab extends SkTabImpl {

    get prefix() {
        return 'default';
    }

    get suffix() {
        return 'tab';
    }

}
