
import { SkTabsImpl }  from '../../sk-tabs/src/impl/sk-tabs-impl.js';

import { OPEN_AN, TITLE_AN } from "../../sk-tabs/src/sk-tabs.js";

import { DISABLED_AN } from "../../sk-core/src/sk-element.js";

export class DefaultSkTabs extends SkTabsImpl {

    get prefix() {
        return 'default';
    }

    get suffix() {
        return 'tabs';
    }

    get tabsEl() {
        if (! this._tabsEl) {
            this._tabsEl = this.comp.el.querySelector('div#tabs');
        }
        return this._tabsEl;
    }

    set tabsEl(el) {
        this._tabsEl = el;
    }

    get titleBarEl() {
        if (! this._tabsTitleBarEl) {
            this._tabsTitleBarEl = this.tabsEl.querySelector('.tabs-title-list');
        }
        return this._tabsTitleBarEl;
    }

    set titleBarEl(el) {
        this._tabsTitleBarEl = el;
    }

    get tabsContentEl() {
        if (! this._titleContentEl) {
            this._titleContentEl = this.tabsEl.querySelector('.tabs-contents');
        }
        return this._titleContentEl;
    }

    set tabsContentEl(el) {
        this._titleContentEl = el;
    }

    get subEls() {
        return [ 'tabsEl', 'titleBarEl', 'tabsContentEl' ];
    }

    updateTabs(curTabId) {
        for (let tabId of Object.keys(this.tabs)) {
            let tab = this.titleBarEl.querySelector(`[data-tab-id=${tabId}]`);
            if (tabId === curTabId) {
                let activeTabPane = this.tabs[tabId];
                activeTabPane.style.display = 'block';
                tab.classList.add('sk-tabs-active');
            } else {
                this.tabs[tabId].style.display = 'none';
                tab.classList.remove('sk-tabs-active');
            }
        }
    }

    renderTabs(tabEls) {
        if (this.comp.isTplSlotted) {
            this.renderSlottedTabs(tabEls);
        } else {
            let tabs = tabEls || this.tabsEl.querySelectorAll(this.comp.tabSl);
            let num = 1;
            this.tabs = {};
            for (let tab of tabs) {
                let isOpen = tab.hasAttribute(OPEN_AN);
                let title = tab.getAttribute(TITLE_AN) ? tab.getAttribute(TITLE_AN) : '';
                this.titleBarEl.insertAdjacentHTML('beforeend', `
                    <div role="tab" ${isOpen ? 'open' : ''} data-tab-id="${'tabs-' + num}" class="${isOpen ? 'sk-tabs-active' : ''}  ${tab.hasAttribute(DISABLED_AN) ? 'sk-tab-disabled' : ''}"> 
                        <a href="#tabs-${num}">
                            ${title}
                        </a>
                    </div>`);
                this.tabsContentEl.insertAdjacentHTML('beforeend', `
                    <div id="tabs-${num}" ${! isOpen ? 'style="display: none;"' : ''}>
                        ${tab.outerHTML}
                    </div>
                `);
                this.removeEl(tab);
                this.tabs['tabs-' + num] = this.tabsEl.querySelector('#tabs-' + num);
                num++;
            }
        }
    }

    bindTabSwitch() {
        this.titleBarEl.querySelectorAll('a').forEach(function(link) {
            link.onclick = function(event) {
                if (this.comp.hasAttribute(DISABLED_AN)
                    || event.target.classList.contains('sk-tab-disabled')
                    || event.target.parentElement.classList.contains('sk-tab-disabled')) {
                    return false;
                }
                let tabId = event.target.getAttribute('href').substr(1);
                this.updateTabs(tabId);
            }.bind(this);
        }.bind(this));
    }

    afterRendered() {
        super.afterRendered();
        this.restoreState({ contentsState: this.contentsState || this.comp.contentsState });
        this.mountStyles();
    }
}
