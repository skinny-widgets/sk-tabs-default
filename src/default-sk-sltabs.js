
import { SkSltabsImpl }  from '../../sk-tabs/src/impl/sk-sltabs-impl.js';

export class DefaultSkSltabs extends SkSltabsImpl {

    get prefix() {
        return 'default';
    }

    get suffix() {
        return 'sltabs';
    }

}
